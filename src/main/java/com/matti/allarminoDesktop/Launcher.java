package com.matti.allarminoDesktop;

public class Launcher {
	public static void main(String[] args) {
		Config config = new Config("config.toml");
		UtilsMQTT.setConfig(config);
		ManagerMQTT manager = new ManagerMQTT();
		ClientMQTT client = new ClientMQTT(UtilsMQTT.config.getServerName(), UtilsMQTT.config.getServerPort(), UtilsMQTT.config.getUserName(), UtilsMQTT.config.getUserPass(), manager);
		UtilsMQTT.setClient(client);
		UtilsMQTT.client.subscribe(UtilsMQTT.config.getInAllarme(), UtilsMQTT.config.getInPre(), UtilsMQTT.config.getNotifiche(), UtilsMQTT.config.getAllarmeAttivo());
		App.main(args);
	}
}
