package com.matti.allarminoDesktop;

public class Allarme {
	private static boolean inAllarme, inPre, allarmeAttivo;

	public static boolean isInAllarme() {
		return inAllarme;
	}

	public static void setInAllarme(boolean inAllarme) {
		Allarme.inAllarme = inAllarme;
		App.updateInAllarme();
	}

	public static boolean isInPre() {
		return inPre;
	}

	public static void setInPre(boolean inPre) {
		Allarme.inPre = inPre;
		App.updateInPre();
	}

	public static boolean isAllarmeAttivo() {
		return allarmeAttivo;
	}

	public static void setAllarmeAttivo(boolean allarmeAttivo) {
		Allarme.allarmeAttivo = allarmeAttivo;
		App.updateAllarmeAttivo();
	}
}
