package com.matti.allarminoDesktop;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javafx.application.*;
import javafx.geometry.Insets;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.Scene;
import javafx.scene.control.*;


public class App extends Application {
	public static boolean started = false;
	private static final int LARGHEZZA = 870;
	private static final int ALTEZZA = LARGHEZZA/2;
	
	private static final float VPADDING = ALTEZZA/15;
	private static final float HPADDING = LARGHEZZA/20;

	private static final float ALTEZZA_BUTTON = ALTEZZA/6;
	private static final float LARGHEZZA_BUTTON = LARGHEZZA/6;
	
	private static final String ROSSO = "#FF3414";
	private static final String GIALLO = "#EBC026";
	private static final String VERDE = "#26EB5D";
	
	static Button suonoAllarme, preAllarme, allarmeAttivo;
	static Button allarmeIstantaneo, attivaAllarme, disattivaAllarme;
	static TextArea boxNotifiche;
	
	GridPane gp1, gp2;
	Insets buttonPadding, notifichePadding;
	
	
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		buttonPadding = new Insets(VPADDING, HPADDING, VPADDING, HPADDING);
		notifichePadding = new Insets(VPADDING);
		
		suonoAllarme = new Button("Suono allarme");
		preAllarme = new Button("PreAllarme");
		allarmeAttivo = new Button("Allarme A/D");
		
		allarmeIstantaneo = new Button("Allarme Istantaneo");
		attivaAllarme = new Button("Attiva Allarme");
		disattivaAllarme = new Button("Disattiva Allarme");
		
		allarmeIstantaneo.setOnAction(e -> ali());
		attivaAllarme.setOnAction(e -> ala());
		disattivaAllarme.setOnAction(e -> ald());
		suonoAllarme.setMinHeight(ALTEZZA_BUTTON);
		suonoAllarme.setMinWidth(LARGHEZZA_BUTTON);
		preAllarme.setMinHeight(ALTEZZA_BUTTON);
		preAllarme.setMinWidth(LARGHEZZA_BUTTON);
		allarmeAttivo.setMinHeight(ALTEZZA_BUTTON);
		allarmeAttivo.setMinWidth(LARGHEZZA_BUTTON);
		allarmeIstantaneo.setMinHeight(ALTEZZA_BUTTON);
		allarmeIstantaneo.setMinWidth(LARGHEZZA_BUTTON);
		attivaAllarme.setMinHeight(ALTEZZA_BUTTON);
		attivaAllarme.setMinWidth(LARGHEZZA_BUTTON);
		disattivaAllarme.setMinHeight(ALTEZZA_BUTTON);
		disattivaAllarme.setMinWidth(LARGHEZZA_BUTTON);
		
		boxNotifiche = new TextArea();
		boxNotifiche.setEditable(false);
		
		gp1 = new GridPane();
		gp2 = new GridPane();
		
		gp1.add(suonoAllarme, 0, 0);
		gp1.add(preAllarme, 1, 0);
		gp1.add(allarmeAttivo, 0, 1);
		gp1.add(allarmeIstantaneo, 1, 1);
		gp1.add(attivaAllarme, 0, 2);
		gp1.add(disattivaAllarme, 1, 2);
		
		gp2.add(gp1, 0, 0);
		gp2.add(boxNotifiche, 1, 0);
		
		gp1.setHgap(HPADDING);
		gp1.setVgap(VPADDING);
		gp1.setPadding(buttonPadding);
		gp2.setPadding(notifichePadding);
		
		Scene sc = new Scene(gp2, LARGHEZZA, ALTEZZA);
		
		primaryStage.setScene(sc);
		primaryStage.setResizable(false);
		
		primaryStage.setTitle("Client Allarmino");
		
		started = true;
		
		primaryStage.show();
	}
	
	public static void ala() {
		UtilsMQTT.client.invia(UtilsMQTT.config.getInviaComando(), UtilsMQTT.config.getComandoAttivazione());
	}
	
	public static void ald() {
		UtilsMQTT.client.invia(UtilsMQTT.config.getInviaComando(), UtilsMQTT.config.getComandoDisattivazione());
	}
	
	public static void ali() {
		UtilsMQTT.client.invia(UtilsMQTT.config.getInviaComando(), UtilsMQTT.config.getComandoIstantaneo());
	}
	
	public static void updateInAllarme() {
		if (Allarme.isInAllarme()) 
			suonoAllarme.setStyle("-fx-background-color: " + ROSSO + "; -fx-text-fill: white");
		else
			suonoAllarme.setStyle("-fx-background-color: " + VERDE + "; -fx-text-fill: black");	
		
	}
	
	public static void updateInPre() {
		if (Allarme.isInPre()) 
			preAllarme.setStyle("-fx-background-color: " + GIALLO);
		else
			preAllarme.setStyle("-fx-background-color: " + VERDE);
	}
	
	public static void updateAllarmeAttivo() {
		if (Allarme.isAllarmeAttivo()) 
			allarmeAttivo.setStyle("-fx-background-color: " + ROSSO + "; -fx-text-fill: white");
		else 
			allarmeAttivo.setStyle("-fx-background-color: " + VERDE + "; -fx-text-fill: black");
	}
	
	public static void log(String msg) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH.mm.ss");
		LocalDateTime now = LocalDateTime.now();
		boxNotifiche.appendText(String.format("[%s]: \"%s\"%n", dtf.format(now), msg));
	}
	
	@Override
	public void stop() {
		UtilsMQTT.exit();
	}
}
