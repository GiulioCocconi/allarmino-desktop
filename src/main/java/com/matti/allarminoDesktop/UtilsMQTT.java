package com.matti.allarminoDesktop;

import org.tinylog.Logger;

public class UtilsMQTT {
	public static ClientMQTT client;
	public static Config config;
	
	public static void welcome() {
		Logger.info("Benvenuto nel client AllarminoDesktop!");
	}
	
	public static void setClient(ClientMQTT client) {
		UtilsMQTT.client = client;
	}


	public static void setConfig(Config config) {
		UtilsMQTT.config = config;
	}

	public static void exit() {
		client.disconnect();
		Logger.info("Arrivederci");
	}
	
}
