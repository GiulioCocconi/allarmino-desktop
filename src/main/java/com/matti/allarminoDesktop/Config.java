package com.matti.allarminoDesktop;

import java.io.File;

import org.tinylog.Logger;
import com.moandjiezana.toml.Toml;

public class Config {
	private String serverName;
	private int serverPort;
	
	private String userName, userPass;
	private String prefix;
	private String notifiche, inviaComando, update, inAllarme, inPre, warning, allarmeAttivo;
	private String comandoAttivazione, comandoDisattivazione, comandoIstantaneo, comandoUpdate;
	
	public Config(String path) {
		Logger.info("Avviato parser di configurazione per il file " + path);
		refresh(path);
	}
	
	public void refresh(String path) {
		File file = new File(path);
		Toml toml = new Toml().read(file);
		this.serverName = toml.getString("serverName");
		this.serverPort = Integer.parseInt(toml.getString("serverPort"));
		
		this.userName = toml.getString("userName");
		this.userPass = toml.getString("userPass");
		
		this.prefix = toml.getString("prefix");
		this.notifiche = this.prefix + toml.getString("notifiche");
		this.inviaComando = this.prefix + toml.getString("inviaComando");
		this.update = this.prefix + toml.getString("update");
		this.inAllarme = this.prefix + toml.getString("inAllarme");
		this.inPre = this.prefix + toml.getString("inPre");
		this.warning = this.prefix + toml.getString("warning");
		this.allarmeAttivo = this.prefix + toml.getString("allarmeAttivo");
		
		this.comandoAttivazione = toml.getString("comandoAttivazione");
		this.comandoDisattivazione = toml.getString("comandoDisattivazione");
		this.comandoIstantaneo = toml.getString("comandoIstantaneo");
		this.comandoUpdate = toml.getString("comandoUpdate");
		
		Logger.info("Configurazione aggiornata");
	}

	public String getServerName() {
		return serverName;
	}

	public int getServerPort() {
		return serverPort;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getNotifiche() {
		return notifiche;
	}

	public String getInviaComando() {
		return inviaComando;
	}

	public String getUpdate() {
		return update;
	}

	public String getInAllarme() {
		return inAllarme;
	}

	public String getInPre() {
		return inPre;
	}

	public String getWarning() {
		return warning;
	}

	public String getComandoAttivazione() {
		return comandoAttivazione;
	}

	public String getComandoDisattivazione() {
		return comandoDisattivazione;
	}

	public String getComandoIstantaneo() {
		return comandoIstantaneo;
	}

	public String getComandoUpdate() {
		return comandoUpdate;
	}
	
	public String getAllarmeAttivo() {
		return allarmeAttivo;
	}
}
