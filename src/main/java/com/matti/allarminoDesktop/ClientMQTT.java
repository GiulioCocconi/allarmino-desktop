package com.matti.allarminoDesktop;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.*;
import org.tinylog.Logger;

public class ClientMQTT  {
	final String pubId = UUID.randomUUID().toString();
	private String link;
	private IMqttClient pub;
	private MqttConnectOptions optns;

	public ClientMQTT(String serverName, int serverPort, String username, String password, MqttCallback call) {
		Logger.info("MQTT Manager Started");
		this.link = "tcp://" + serverName + ":" + serverPort;
		try {
			this.pub = new MqttClient(link, pubId);
		} catch (MqttException e) {
			Logger.error(e);
		}
		this.optns = new MqttConnectOptions();
		optns.setAutomaticReconnect(true);
		optns.setConnectionTimeout(10);
		optns.setCleanSession(true);
		optns.setUserName(username);
		optns.setPassword(password.toCharArray());
		try {
			pub.connect(optns);
			pub.setCallback(call);
			Logger.info("Manager connesso a " + username + "@" + serverName);
		} catch (MqttException e) {
			Logger.error(e);
			Logger.error("Controlla la connessione");
		}
	}
	
	public void invia(String topic, String payload) {
		MqttMessage msg = new MqttMessage(payload.getBytes());
		msg.setQos(0);
		msg.setRetained(false);
		try {
			this.pub.publish(topic, msg);
		} catch (MqttException e) {
			Logger.error(e);
		}
		Logger.info("Messaggio inviato: " + msg);
	}
	
	public void subscribe(String... topic) {
		int[] qos = new int[topic.length];
		try {
			pub.subscribe(topic, qos);
		} catch (MqttException e) {
			Logger.error(e);
		}
	}
	public void disconnect() {
		Logger.info("Mi sto disconnettendo da " + this.link);
		try {
			this.pub.disconnect();
		} catch (MqttException e) {
			Logger.error(e);
		}
	}
}
