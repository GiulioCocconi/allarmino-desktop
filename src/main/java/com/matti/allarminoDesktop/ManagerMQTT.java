package com.matti.allarminoDesktop;

import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.paho.client.mqttv3.*;
import org.tinylog.Logger;

public class ManagerMQTT implements MqttCallback {
	
	
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		if (App.started) {
			String mString = new String(message.getPayload());
			int state = -1; //Stato di default se il numero non contiene un numero

			if (NumberUtils.isCreatable(mString)) //Se il payload contiene un numero
				state = Integer.parseInt(mString);

			if (topic.equals(UtilsMQTT.config.getInAllarme())) {
				Logger.info("Set INALLARME to " + state);
				Allarme.setInAllarme(state == 1);
			}
			else if (topic.equals(UtilsMQTT.config.getInPre())) {
				Logger.info("Set INPRE to " + state);
				Allarme.setInPre(state == 1);
			}
			else if (topic.equals(UtilsMQTT.config.getNotifiche())) {
				Logger.info("Notifica: " + mString);
				App.log(mString);
			}
			else if (topic.equals(UtilsMQTT.config.getAllarmeAttivo())) {
				Logger.info("Set ALLARME ATTIVO to" + state);
				Allarme.setAllarmeAttivo(state == 1);
			}
			else {
				Logger.info(String.format("Ricevuto %s da %s", mString, topic));
			}
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		Logger.error("Connessione a MQTT persa...\nL'applicazione verr� chiusa");
		Logger.error(cause);
		UtilsMQTT.exit();
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {}

}
